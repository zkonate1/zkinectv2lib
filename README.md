# zKinectV2lib

The project contains pd-based resources, and a windows64 application.  Note that the pd-resources depend on the library pdSheefa, which can be found here: git@gitlab.com:zkonate1/pdSheefa.git

note: connecting the machine to the network using a CABLE and not via WiFi is generally recommened; however **it is necessary in the case that you are using USB broadcast mode** (which is the default mode).

note:  in oder to run the zkinectV2app the Microsoft kinect v2 STK needs to be installed on your window 10 machine host.  This should automatically be downloaded when the kinect v2 is connected to the windows machine. 



## Getting started



### Using the **zkinectV2app** application 
To launch the app, open the folder **zkinectV2app** locate the file **launch_zKinectV2.bat** and double click on it. 

By default, the application will track 1 user within a distance of 4.5 meters and broadcast the skeleton data on the your network over port 54321   

#### Customizing the application parameters
 Changes to the application behovir are made by editing the file **launch_zKinectV2.bat** file, where the command below is defined:
 
 ```
 %~dp0\x64\Release\zKinectV2OSC.exe
 ```
 
For example, if you want the application to send the osc output to a particular ip address on the network,  you can add a flag the command line in as shown below:

```
 %~dp0\x64\Release\zKinectV2OSC.exe --ip 10.10.1.33
 ```

Or for example, to increase the number of trackable users, disable skeleton joint tracking, use a different port, and reduce the tracking space,  try:

```
%~dp0\x64\Release\zKinectV2OSC.exe  --port 9001 --depthClip 3.45 --maxUsers 4 --jointTX 0
 ```
 
Below are the optional command line arguments that are available:

```
        -p , --port, DefaultValue = 64321

        -i, --ip, DefaultValue = your local network broadcast (e.g. 192.168.0.255)

        -r, --oscUpdateRateMs, DefaultValue = 50 ms

        -d,  --depthClip", DefaultValue = 4.5 meters   (maximum tracking distance from the kinect)

        -j, --jointTX, DefaultValue = 1 (enable/disable osc joint messages)

        -m, --maxUsers, DefaultValue = 1 user  (maximum number of users tracked)
```





 For a description of the set of OSC messages the application can send, click here: [https://gitlab.com/zkonate1/zKinectV2osc2022/-/blob/master/README.md](https://gitlab.com/zkonate1/zKinectV2osc2022/-/blob/master/README.md)
