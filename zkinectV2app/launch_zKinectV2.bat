
REM  	note: Comments are proceeded with the word REM.
REM	Below is the command line that will open the application. Modify it to suit your needs.

 %~dp0\x64\Release\zKinectV2OSC.exe --port 54321 --depthClip 4.45 --maxUsers 1



REM 	Below is an example of a command line that tracks up to 6 users and that does not send joint information. Messages will be sent to a receiver located here: (192.168.0.238)
REM	%~dp0\x64\Release\zKinectV2OSC.exe --ip 192.168.0.238 --port 54321 --depthClip 4.45 --maxUsers 6 --jointTX 0


REM options:

REM     -p , --port, DefaultValue = 54321

REM     -i, --ip, DefaultValue = local network broadcast (e.g. 192.168.0.255)

REM     -r, --oscUpdateRateMs, DefaultValue = 50 ms

REM     -d,  --depthClip", DefaultValue = 4.5 meters

REM     -j, --jointTX,  DefaultValue = 1 (enable/disable osc joint messages)
			
REM	-m, --maxUsers, DefaultValue = 1 user  (maximum number is 6)


REM	-i xxx.xxx.xxx.xxx    network address of OSC receiver,  default: your-local-network_Broadcast
REM	-p  n	where n is the port number the OSC receiver is listening on,  default:   54321

