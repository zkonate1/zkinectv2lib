edit the file  launch_zKinectV2.bat to set the IP address of the machine where airjam will be running

note:  in oder to use the kinect v2,  you will need to install the Microsoft kinect v2 STK on the window 10 machine host.  This should be automatically downloaded when the kinect v2 is connected to the windows machine. 


zKinectV2osc was built on the project:   https://github.com/microcosm/KinectV2-OSC


Below are additional command line options that the program can use:


Global status message once per second:

```sh
Address: /kinect2/status
Values: - float:  depthClipMeters
```

Joints Messages, in local coordinates, relative to the SpineMid joint

Modified message protocol for joints:

```sh
Address: /kinect2/{bodyId: 0 - 5 }/joint/{jointId}
Values: - float:  positionX
        - float:  positionY
        - float:  positionZ
```
and message protocol for hands:

```sh
Address: /kinect2/{bodyId: 0 - 5 }/hands/Left or  ..../Right 
Value - symbol:  Open, CLosed, or NotTracked
```
New messages for:

6DoF User pose: position and orientation (relative to SpineMid)

```sh
Address: /kinect2/{bodyId: 0 - 5 }/6dof
Values: - float:  positionX
        - float:  positionY
        - float:  positionZ
        - float:  quaternianX
        - float:  quaternianY
        - float:  quaternianZ
        - float:  quaternianW
        
 Address: /kinect2/{bodyId: 0 - 5 }/lean        
        - float:  leanFrontRear
        - float: leanLeftRight
```

Skeletal pose frame synchronization (output each frame boundry)

```sh
Address: /kinect2/{bodyId: 0 - 5 }/skelFrame
```

There are also a some command line argument flags

            -p , --port, DefaultValue = 12345


            -i, --ip, DefaultValue = local network broadcast (e.g. 192.168.0.255)


            -r, --oscUpdateRateMs, DefaultValue = 50 ms


            -d,  --depthClip", DefaultValue = 4.5 meters


            -j, --jointTX, DefaultValue = 1 (enable/disable osc joint messages)

Other:
				UDB broadcast has been observed to fail over Wifi